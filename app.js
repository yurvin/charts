const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const MainData = require('./scripts/MainData');
const IndexRouter = require('./routes/index');
const app = express();


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'src')));

app.mainData = new MainData();
app.use('/', new IndexRouter(app));

module.exports = app;
