const express = require('express');
const router = express.Router();
const ip = require('ip');
const CONSTANTS = require('../utils/CONSTANTS');

module.exports = class {
    constructor(app) {
        let urls = CONSTANTS.urls;
        let title = CONSTANTS.app.title;
        let mainData = app.mainData ? app.mainData : null;
        console.log('mainData',mainData)
        this.requestHandlers(urls, title, mainData);

        return router;
    }

    requestHandlers(urls, title, mainData) {
        router.get(urls.entry, (req, res, next) => res.render('index', { title: title }) );

        router.get(urls.ip, (request, response) => {
            if(!request.body) return response.sendStatus(400);
            console.log(request.body);
            response.send({ ip: ip.address() });
        });

        router.get(urls.pairs, (request, response) => {
            if(!request.body) return response.sendStatus(400);
            console.log(request.body);
            response.send({ pairs: mainData.quots.getPairs() });
        });
    }
};