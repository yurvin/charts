const Quots = require('./Quots');
const wsServer = require('./helper/wsServer');
const EventEmitter = require('events').EventEmitter;

module.exports = class MainData {
  constructor() {
    let ee = new EventEmitter;

    this.quots = new Quots(ee);
    this.wsS = new wsServer(ee);

    this.setListeners(ee);
  }

  setListeners(ee) {
    ee.on('getData', (params) => {
        let data = this.quots.getData(params);
        console.log('data', data);
        ee.emit('sendData', data, params.client);
    });

    ee.on('sendData', (data, client) => {
        this.wsS.send(data, client);
    });
  }
}
