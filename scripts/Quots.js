const _ = require('lodash');
const utils = require('./helper/utils');

module.exports = class Quots {

  constructor(ee) {
    console.log('call constructor Quots');
    this.pairsData = {
      'EUR/USD': []
    };

    this.pairsData = this.historyGenerate(this.pairsData, 600);
    this.dataUpdate(this.pairsData, ee);
  }

  historyGenerate(pairsData, count) {
    for (let pair in pairsData) {
      let data = pairsData[pair];

      data = new Array(count);
      let time = new Date().getTime();

      for (let i = count-1; i >= 0; i--) {
          data[i] = this.generateQuot(time);
          time = time - 1000;
      }
      pairsData[pair] = data;
    }
    console.log('pairsData', pairsData);
    return pairsData;
  }

  dataUpdate(pairsData, ee) {
      let promise = this.newQuot();
      promise.then((quot) => {
          pairsData['EUR/USD'].push(quot);
          console.log('qout', quot);
          ee.emit('sendData', quot);
          this.dataUpdate(this.pairsData, ee);
      }, (error) => {
          console.log('error', error);
      });

  }

  generateQuot(time) {
    return {
      time: _.isNumber(time) ? time : new Date().getTime(),
      quot: utils.math.getRandomArbitrary(0, 100)
    };
  }

  newQuot() {
    let frequency = 100;
    let dataQuots = new Array(10);
    let promise = null;

    let summaryQuots = (arr) => {
      let summaryQuots = arr.reduce((sum, current) => {
        return sum + current.quot;
      }, 0);

      return {
        quot: summaryQuots / dataQuots.length,
        time: dataQuots[dataQuots.length - 1].time
      };
    };


    let generateQuotMin = () => {
      promise = new Promise((resolve, reject) => {
        let index = 0;
        let intervalId = setInterval(() => {

          if (index >= 10) {
            clearInterval(intervalId);
            let summaryQuot = summaryQuots(dataQuots);

            resolve(summaryQuot);
          }

          dataQuots[index] = this.generateQuot();
          index++;
        }, frequency);
      });
    };

    generateQuotMin();

    return promise;
  }

  getData(params) {
    if (
      !params || !params.request ||
      typeof this.pairsData[params.request.pair] === 'Array'
    ) { return null; }

    let request = params.request;
    let dataQuot = this.pairsData[request.pair];

    if (request.count) {
      return dataQuot.slice(dataQuot.length - request.count - 1, dataQuot.length - 1);
    } else if (request.from && request.to) {
      return dataQuot.slice(dataQuot[request.from], dataQuot[request.to]);
    }

    return null;
  }

  getPairs() {
    return _.keys(this.pairsData);
  }
};
