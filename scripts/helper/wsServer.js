#!/usr/bin/env node
const WebSocketServer = require('websocket').server;
const http = require('http');

module.exports = class {
    constructor(ee) {
        this.clients = [];
        let server = this.createHttpServer(8080);
        let wsServer = this.createWsServer(server);
        this.setListenersServer(wsServer, ee);
    }

    setListenersServer(wsServer, ee) {
        wsServer.on('request', (request) => {
            if (!this.originIsAllowed(request.origin)) {
                // Make sure we only accept requests from an allowed origin
                request.reject();
                console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
                return;
            }

            this.connection = request.accept(null, request.origin);
            var index = this.clients.push(this.connection) - 1;

            this.connection.on('message', (message) => {
                console.log('message', message);
                if (message.type === 'utf8') {

                    console.log('message.utf8Data', message.utf8Data);
                    ee.emit('getData', {
                      client: this.connection,
                      request: JSON.parse(message.utf8Data)
                    });
                }
            });

            this.connection.on('close', (reasonCode, description) => {
                console.log((new Date()) + ' Peer ' + this.connection.remoteAddress + ' disconnected.');
                this.clients.splice(index, 1);
            });
        });
    }

  createHttpServer(port) {
    var server = http.createServer((request, response) => {
        console.log((new Date()) + ' Received request for ' + request.url);
        response.writeHead(404);
        response.end();
    });
    server.listen(port, () => {
        console.log((new Date()) + ' Server is listening on port ' + port);
    });

      return server;
    }

    createWsServer(httpServer) {
        return new WebSocketServer({
            httpServer: httpServer,
            // You should not use autoAcceptConnections for production
            // applications, as it defeats all standard cross-origin protection
            // facilities built into the protocol and the browser.  You should
            // *always* verify the connection's origin and decide whether or not
            // to accept it.
            autoAcceptConnections: false
        });
    }

    originIsAllowed(origin) {
        // put logic here to detect whether the specified origin is allowed.
        return true;
    }

    send(data, client) {
      let sendUTF = (data, client1) => {
        if (client1 && client1.sendUTF) {
            client1.sendUTF(JSON.stringify(data));
        }
      }

      if (client) {
        sendUTF(data, client);
      } else {
        console.log('clients', this.clients.length);
        this.clients.forEach((client) => { sendUTF(data, client); });
      }

    }
};
