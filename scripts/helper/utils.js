const _ = require('lodash');

module.exports = {
  math: {
    /**
      Возвращает случайное число между 0 (включительно) и 1 (не включая 1)
    */
    getRandom: () => {
      return Math.random();
    },

    /**
      Возвращает случайное число между min (включительно) и max (не включая max)
    */
    getRandomArbitrary: (min, max) => {
      return Math.random() * (max - min) + min;
    },
    /**
      Возвращает случайное целое число между min (включительно) и max (не включая max)
      Использование метода Math.round() даст неравномерное распределение!
    */
    getRandomInt: (min, max) => {
      return Math.floor(Math.random() * (max - min)) + min;
    }
  }
};
