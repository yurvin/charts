import Data from './dataCommunication/Data';
import CONSTANTS from './helper/CONSTANTS.json';
import ajax from './helper/ajax';
import * as PIXI from 'pixi.js';

class App {
  constructor() {
      this.data = new Data(this);
      this.get(CONSTANTS.urls.ip, 'ip', this.data.setIp.bind(this.data));
      this.get(CONSTANTS.urls.pairs, 'pairs', this.data.setPairs.bind(this.data));
  }

  get(url, key, callback) {
      ajax.get(url).then((res) => {
          console.log('res', res);
          document.getElementById("ip").innerHTML = res;
          res = JSON.parse(res);
          if(res[key]) {
              callback(res[key]);
          }
      });
  }

}
window.chartLib = new App ();
