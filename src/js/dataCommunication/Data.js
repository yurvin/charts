import Websocket from '../helper/Websocket';

module.exports = class Data {
  constructor() {
    this.pairs = [];
  }

  createWS(ip) {
    return new Websocket(ip);
  }

  setIp(ip) {
      this.ws = this.createWS(ip);
  }

  //список пар
  setPairs(pairs) {
      this.pairs = pairs;
      this.getPairData({
          // pair: 'EUR/USD',
          pair: this.pairs[0],
          count: 600
      });
  }

  //котировки пары
  getPairData(params) {
    let intervalId =  setInterval(() => {
      if (this.ws.client.readyState === this.ws.client.OPEN) {
        clearInterval(intervalId);
        this.ws.client.send(JSON.stringify(params));
      }
    }, 10);


  }
};
