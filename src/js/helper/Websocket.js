import { w3cwebsocket as W3CWebSocket }  from 'websocket';

module.exports = class  {
    constructor(address, port) {
        if (!address) return null;
        port = port ? port : '8080';
        this.client = new W3CWebSocket('ws://' + address + ':' + port + '/');
        this.client = this.setListeners(this.client);
    }

    send(message) {
       if (this.client.readyState === this.client.OPEN) {
         this.client.send(message);
       }
    }

    setListeners(client){
        client.onerror = function() {
            console.log('Connection Error');
        };

        client.onopen = function() {
            console.log('WebSocket Client Connected');
        };

        client.onclose = function() {
            console.log('echo-protocol Client Closed');
        };

        client.onmessage = function(e) {
          console.log('e', e);
            if (typeof e.data === 'string') {
                console.log("Received: '" + e.data + "'");
                document.getElementById("wsRes").innerHTML = e.data;
            }
        };

        return client;
    }
};
